/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alumnos;

import java.util.Scanner;

/**
 *
 * @author Ascanio-Duran
 */
public class Operaciones1 {
    
    Scanner Leer = new Scanner(System.in);
   
    double[] mate = new double[4];
    double[] fis = new double[4];
    double[] prog = new double[4];
    double[] quim = new double[4];
   
    String nombres []= new String[5];
    double promedio []= new double[5];
    Operaciones1 alumno[] = new Operaciones1[2];
    
    double calificaciones [][];
    
    private String nombre;
    private String apellido;
    private long identificacion;
    private int edad;
    
    private double p1;
    private double p2;
    private double p3;
    private double p4;
    private double prom; 
    
    private double p1fis;
    private double p2fis;
    private double p3fis;
    private double p4fis;
    private double prom2;
    
    private double p1prog;
    private double p2prog;
    private double p3prog;
    private double p4prog;
    private double prom3;
    
    private double p1quim;
    private double p2quim;
    private double p3quim;
    private double p4quim;
    private double prom4;
    
    public Operaciones1()
    {
        nombre = "";
        apellido = "";
        identificacion = 0;
        edad = 0;
        p1 = 0;
        p2 = 0;
        p3 = 0;
        p4 = 0;
        prom = 0;
       
        p1fis = 0;
        p2fis = 0;
        p3fis = 0;
        p4fis = 0;
        prom2 = 0;
        
        p1prog = 0;
        p2prog = 0;
        p3prog = 0;
        p4prog = 0;
        prom3 = 0;
        
        p1quim = 0;
        p2quim = 0;
        p3quim = 0;
        p4quim = 0;
        prom4 = 0;
    }
    
    public Operaciones1(String n, String a, long id, int e, double p11, double p22, double p33, double p44, double prom1, double pfis1, double pfis2, double pfis3, double pfis4, double prome2, double pprog1, double pprog2, double pprog3, double pprog4, double prome3, double pquim1, double pquim2, double pquim3, double pquim4, double prome4) {
        
        nombre = n;
        apellido = a;
        identificacion = id;
        edad = e;
        p1 = p11;
        p2 = p22;
        p3 = p33;
        p4 = p44;
        prom = prom1;
        
        p1fis = pfis1;
        p2fis = pfis2;
        p3fis = pfis3;
        p4fis = pfis4;
        prom2 = prome2; 
        
        p1prog = pprog1;
        p2prog = pprog2;
        p3prog = pprog3;
        p4prog = pprog4;
        prom3 = prome3; 
        
        p1prog = pquim1;
        p2prog = pquim2;
        p3prog = pquim3;
        p4prog = pprog4;
        prom4 = prome4; 
        
    }
    
     public void pedirDatos() {
    
        for (int i = 0; i < alumno.length; i++) {
                System.out.println("Ingresa el nombre del "+(i+1)+" alumno");
                nombre = Leer.next();
                System.out.println("Ingresa el apellido del alumno");
                apellido = Leer.next();
                System.out.println("Ingresa la identificación del alumno");
                identificacion = Leer.nextLong();
                System.out.println("Ingresa la edad del alumno");
                edad = Leer.nextInt();
                
                System.out.println();
                
                System.out.println("Ingrese la nota del p1 de matemáticas");
                p1 = Leer.nextDouble();
                p1 = (p1*25)/100;
                System.out.println("Ingrese la nota del p2 de matemáticas");
                p2 = Leer.nextDouble();
                p2 = (p2*25)/100;
                System.out.println("Ingrese la nota del p3 de matemáticas");
                p3 = Leer.nextDouble();
                p3 = (p3*20)/100;
                System.out.println("Ingrese la nota del p4 de matemáticas");
                p4 = Leer.nextDouble();
                p4 = (p4*30)/100;
                prom = (p1+p2+p3+p4);
                
                System.out.println();
                
                System.out.println("Ingrese la nota del p1 de fisica");
                p1fis = Leer.nextDouble();
                p1fis = (p1fis*25)/100;
                System.out.println("Ingrese la nota del p2 de fisica");
                p2fis = Leer.nextDouble();
                p2fis = (p2fis*25)/100;
                System.out.println("Ingrese la nota del p3 de fisica");
                p3fis = Leer.nextDouble();
                p3fis = (p3fis*20)/100;
                System.out.println("Ingrese la nota del p4 de fisica");
                p4fis = Leer.nextDouble();
                p4fis = (p4fis*30)/100;
                prom2 = (p1fis+p2fis+p3fis+p4fis);
                
                System.out.println();
                
                System.out.println("Ingrese la nota del p1 de programación");
                p1prog = Leer.nextDouble();
                p1prog = (p1prog*25)/100;
                System.out.println("Ingrese la nota del p2 de programación");
                p2prog = Leer.nextDouble();
                p2prog = (p2prog*25)/100;
                System.out.println("Ingrese la nota del p3 de programación");
                p3prog = Leer.nextDouble();
                p3prog = (p3prog*20)/100;
                System.out.println("Ingrese la nota del p4 de programación");
                p4prog = Leer.nextDouble();
                p4prog = (p4prog*30)/100;
                prom3 = (p1prog+p2prog+p3prog+p4prog);
                
                System.out.println();
                
                System.out.println("Ingrese la nota del p1 de quimica");
                p1quim = Leer.nextDouble();
                p1quim = (p1quim*25)/100;
                System.out.println("Ingrese la nota del p2 de quimica");
                p2quim = Leer.nextDouble();
                p2quim = (p2quim*25)/100;
                System.out.println("Ingrese la nota del p3 de quimica");
                p3quim = Leer.nextDouble();
                p3quim = (p3quim*20)/100;
                System.out.println("Ingrese la nota del p4 de quimica");
                p4quim = Leer.nextDouble();
                p4quim = (p4quim*30)/100;
                prom4 = (p1quim+p2quim+p3quim+p4quim);
                System.out.println();
                alumno[i] = new Operaciones1(nombre, apellido, identificacion, edad, p1, p2, p3, p4, prom, p1fis, p2fis, p3fis, p4fis, prom2, p1prog, p2prog, p3prog, p4prog, prom3, p1quim, p2quim, p3quim, p4quim, prom4);
                System.out.println();
               
              }
        }
         
        
    
     
    public void imprimirArray() {
         System.out.println("Nombre"+"    Apellido"+"      Identificacion"+"   Edad"+"  Prom Mat"+"   Prom Fis"+ "   Prom Progra"+ "   Prom Quim");
            System.out.println();
            for (int a = 0; a < alumno.length; a++){
                System.out.println(alumno[a].getNombre()+"     "+alumno[a].getApellido()+"        "+alumno[a].getIdentificacion()+" "
                        + "      "+alumno[a].getEdad()+"       "+alumno[a].getProm()+"       "+alumno[a].getProm2()+"       "+alumno[a].getProm3()+"       "+alumno[a].getProm4());
                nombres[a] = alumno[a].getNombre();
                mate[a] = alumno[a].getProm();
                fis[a] = alumno[a].getProm2();
                prog[a] = alumno[a].getProm3();
                quim[a] = alumno[a].getProm4(); 
            }
           System.out.println();
   }
    
    public void MayorMat() {
        
        double mayorMat;
             int pos;
             mayorMat=mate[0];
             pos=0;
             
             
             for(int f=0;f<mate.length;f++) {
             if (mate[f]>mayorMat) {
                mayorMat=mate[f];
                pos=f;
               }
             }
             
             
            System.out.println("El estudiante con el mayor promedio en matemáticas es "+nombres[pos]);
            System.out.println("Tiene un promedio de: "+mayorMat);
            System.out.println();
    }
    
    public void MayorFis() {
        
        double mayorfis;
             int pos;
             mayorfis=fis[0];
             pos=0;
             
             
             for(int f=0;f<fis.length;f++) {
             if (fis[f]>mayorfis) {
                mayorfis=fis[f];
                pos=f;
               }
             }
             
             
            System.out.println("El estudiante con el mayor promedio en fisica es "+nombres[pos]);
            System.out.println("Tiene un promedio de: "+mayorfis);
            System.out.println();
    }
    
    public void MayorProg() {
        
        double mayorprog;
             int pos;
             mayorprog=prog[0];
             pos=0;
             
             
             for(int f=0;f<prog.length;f++) {
             if (prog[f]>mayorprog) {
                mayorprog=prog[f];
                pos=f;
               }
             }
             
             
            System.out.println("El estudiante con el mayor promedio en programación es "+nombres[pos]);
            System.out.println("Tiene un promedio de: "+mayorprog);
            System.out.println();
    }
    
    public void MayorQuim() {
        
        double mayorquim;
             int pos;
             mayorquim=quim[0];
             pos=0;
             
             
             for(int f=0;f<quim.length;f++) {
             if (quim[f]>mayorquim) {
                mayorquim=quim[f];
                pos=f;
               }
             }
             
             
            System.out.println("El estudiante con el mayor promedio en quimica es "+nombres[pos]);
            System.out.println("Tiene un promedio de: "+mayorquim);
            System.out.println();
    }
    
    public void MenorMat() {
        
             double menormat;
             int pos;
             menormat=mate[0];
             pos=9999;
             
             
             for(int f=0;f<mate.length;f++) {
             if (mate[f]<menormat) {
                menormat=mate[f];
                pos=f;
               }
             }
             
             
            System.out.println("El estudiante con el menor promedio en matemáticas es "+nombres[pos]);
            System.out.println("Tiene un promedio de: "+menormat);
            System.out.println();
    }
    
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * @return the 
     */
    public long getIdentificacion() {
        return identificacion;
    }

    /**
     * @param identificacion the  to set
     */
    public void setIdentificacion(long identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * @return the 
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * @return the p1
     */
    
    public double getP1() {
        return p1;
    }

    /**
     * @param p1 the p1 to set
     */
    public void setP1(double p1) {
        this.p1 = p1;
        mate[0] = p1;
    }

    /**
     * @return the p2
     */
    public double getP2() {
        return p2;
    }

    /**
     * @param p2 the p2 to set
     */
    public void setP2(double p2) {
        this.p2 = p2;
        mate[1] = p2;
    }

    /**
     * @return the p3
     */
    public double getP3() {
        return p3;
    }

    /**
     * @param p3 the p3 to set
     */
    public void setP3(double p3) {
        this.p3 = p3;
        mate[2] = p3;
    }

    /**
     * @return the p4
     */
    public double getP4() {
        return p4;
    }

    /**
     * @param p4 the p4 to set
     */
    public void setP4(double p4) {
        this.p4 = p4;
        mate[3] = p4;
    }

    /**
     * @return the prom
     */
    public double getProm() {
        return (double)prom;
    }

    /**
     * @param prom the prom to set
     */
    public void setProm(double prom) {
        this.prom = prom;
    }

    /**
     * @return the p1fis
     */
    public double getP1fis() {
        return p1fis;
    }

    /**
     * @param p1fis the p1fis to set
     */
    public void setP1fis(double p1fis) {
        this.p1fis = p1fis;
        fis[0] = p1fis;
    }

    /**
     * @return the p2fis
     */
    public double getP2fis() {
        return p2fis;
    }

    /**
     * @param p2fis the p2fis to set
     */
    public void setP2fis(double p2fis) {
        this.p2fis = p2fis;
        fis[1] = p2fis;
    }

    /**
     * @return the p3fis
     */
    public double getP3fis() {
        return p3fis;
    }

    /**
     * @param p3fis the p3fis to set
     */
    public void setP3fis(double p3fis) {
        this.p3fis = p3fis;
        fis[2] = p3fis;
    }

    /**
     * @return the p4fis
     */
    public double getP4fis() {
        return p4fis;
    }

    /**
     * @param p4fis the p4fis to set
     */
    public void setP4fis(double p4fis) {
        this.p4fis = p4fis;
        fis[3] = p4fis;
    }

    /**
     * @return the prom2
     */
    public double getProm2() {
        return (double)prom2;
    }

    /**
     * @param prom2 the prom2 to set
     */
    public void setProm2(double prom2) {
        this.prom2 = prom2;
    }

    /**
     * @return the p1prog
     */
    public double getP1prog() {
        return p1prog;
    }

    /**
     * @param p1prog the p1prog to set
     */
    public void setP1prog(double p1prog) {
        this.p1prog = p1prog;
        prog[0] = p1prog;
    }

    /**
     * @return the p2prog
     */
    public double getP2prog() {
        return p2prog;
    }

    /**
     * @param p2prog the p2prog to set
     */
    public void setP2prog(double p2prog) {
        this.p2prog = p2prog;
        prog[1] = p2prog;
    }

    /**
     * @return the p3prog
     */
    public double getP3prog() {
        return p3prog;
    }

    /**
     * @param p3prog the p3prog to set
     */
    public void setP3prog(double p3prog) {
        this.p3prog = p3prog;
        prog[2] = p3prog;
    }

    /**
     * @return the p4prog
     */
    public double getP4prog() {
        return p4prog;
    }

    /**
     * @param p4prog the p4prog to set
     */
    public void setP4prog(double p4prog) {
        this.p4prog = p4prog;
        prog[3] = p4prog;
    }

    /**
     * @return the prom3
     */
    public double getProm3() {
        return prom3;
    }

    /**
     * @param prom3 the prom3 to set
     */
    public void setProm3(double prom3) {
        this.prom3 = prom3;
    }

    /**
     * @return the p1quim
     */
    public double getP1quim() {
        return p1quim;
    }

    /**
     * @param p1quim the p1quim to set
     */
    public void setP1quim(double p1quim) {
        this.p1quim = p1quim;
        quim[0] = p1quim;
    }

    /**
     * @return the p2quim
     */
    public double getP2quim() {
        return p2quim;
    }

    /**
     * @param p2quim the p2quim to set
     */
    public void setP2quim(double p2quim) {
        this.p2quim = p2quim;
        quim[1] = p2quim;
    }

    /**
     * @return the p3quim
     */
    public double getP3quim() {
        return p3quim;
    }

    /**
     * @param p3quim the p3quim to set
     */
    public void setP3quim(double p3quim) {
        this.p3quim = p3quim;
        quim[2] = p3quim;
    }

    /**
     * @return the p4quim
     */
    public double getP4quim() {
        return p4quim;
    }

    /**
     * @param p4quim the p4quim to set
     */
    public void setP4quim(double p4quim) {
        this.p4quim = p4quim;
        quim[3] = p4quim;
    }

    /**
     * @return the prom4
     */
    public double getProm4() {
        return prom4;
    }

    /**
     * @param prom4 the prom4 to set
     */
    public void setProm4(double prom4) {
        this.prom4 = prom4;
    }

    /**
     * @return the p1fis
     */
    
 }
        
    
   

  
       

